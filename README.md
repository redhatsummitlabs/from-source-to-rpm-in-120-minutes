# From Source to RPM in 120 Minutes

Here you will find a PDF version of the Lab Guide for the Red Hat Summit 2019
Edition of the "From Source to RPM in 120 Minutes" lab.

This lab guide is heavily based on the [Official Red Hat RPM Packaging
Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/rpm_packaging_guide/index),
to which the upstream version can be found
[here](https://rpm-packaging-guide.github.io/). This documentation is available
in [asciidoc](http://asciidoc.org/) format, along with all example source code [upstream](https://github.com/redhat-developer/rpm-packaging-guide).
